function showQuestion(questions, playingUser,playingTags) {
  var obtainedQuestion=questions;
  var numCorrect = 0;
  var questionCounter = 0; //Tracks question number
  var selections = []; //Array containing user choices
  var quiz = $('#quiz'); //Quiz div object
  var user=playingUser;
  var tag=playingTags;
  
  console.log("Tags selected",playingTags);
  // Display initial question
  displayNext();
  
  // Click handler for the 'next' button
  $('#next').on('click', function (e) {
    e.preventDefault();
    
    // Suspend click listener during fade animation
    if(quiz.is(':animated')) {        
      return false;
    }
    choose();
    
    // If no user selection, progress is stopped
    if (isNaN(selections[questionCounter])) {
      alert('Please make a selection!');
    } else {
      questionCounter++;
      displayNext();
    }
  });
  
  // Click handler for the 'prev' button
  $('#prev').on('click', function (e) {
    e.preventDefault();
    
    if(quiz.is(':animated')) {
      return false;
    }
    choose();
    questionCounter--;
    displayNext();
  });
  
  // Click handler for the 'Start Over' button
  $('#start').on('click', function (e) {
    e.preventDefault();
    
    if(quiz.is(':animated')) {
      return false;
    }
    questionCounter = 0;
    selections = [];
    displayNext();
    $('#start').hide();
  });
  
  // Animates buttons on hover
  $('.button').on('mouseenter', function () {
    $(this).addClass('active');
  });
  $('.button').on('mouseleave', function () {
    $(this).removeClass('active');
  });
  
  // Creates and returns the div that contains the questions and 
  // the answer selections
  function createQuestionElement(index) {
    var qElement = $('<pre>', {
      id: 'question'
    });
    
    var header = $('<h3>Question ' + (index + 1) + ':</h3>');
    qElement.append(header);
    
    var question = $('<span>').append(questions[index].questionString);
    qElement.append(question);
    
    var radioButtons = createRadios(index);
    qElement.append(radioButtons);
    
    return qElement;
  }
  
  // Creates a list of the answer choices as radio inputs
  function createRadios(index) {
    var radioList = $('<ul>');
    var item;
    var input = '';
    for (var i = 0; i < questions[index].options.length; i++) {
      item = $('<li>');
      input =getAlphabetFor(i)+") ";
      input += '<input type="radio" name="answer" value=' + i + ' />';      
      input += questions[index].options[i];
      item.append(input);
      radioList.append(item);
    }
    return radioList;
  }
  
  // Reads the user selection and pushes the value to an array
  function choose() {
    selections[questionCounter] = +$('input[name="answer"]:checked').val();
  }
  
  // Displays next requested element
  function displayNext() {
    quiz.fadeOut(function() {
      $('#question').remove();
      
      if(questionCounter < questions.length){
        var nextQuestion = createQuestionElement(questionCounter);
        quiz.append(nextQuestion).fadeIn();
        if (!(isNaN(selections[questionCounter]))) {
          $('input[value='+selections[questionCounter]+']').prop('checked', true);
        }
        
        // Controls display of 'prev' button
        if(questionCounter === 1){
          $('#prev').show();
          $('#score').hide();
        } else if(questionCounter === 0){
          $('#score').hide();
          $('#prev').hide();
          $('#next').show();
        }
      }else {
        var scoreElem = displayScore();
        quiz.append(scoreElem).fadeIn();
        $('#next').hide();
        $('#prev').hide();
        $('#score').show();
        $('#start').show();
      }
    });
  }
  
  function getAlphabetFor(index)
  {
	  var alphabet = "abcdefghijklmnopqrstuvwxyz".split('');
	  return alphabet[index].toUpperCase();;
  }
  
  // Computes score and returns a paragraph element to be displayed
  function displayScore() {
    var score = $('<p>',{id: 'question'});
    
   
    
    for (var i = 0; i < selections.length; i++) {
    	
    	var correctIndex=obtainedQuestion[i].options.indexOf(obtainedQuestion[i].correct_answer);
    	console.log("Selected "+(i+1)+">"+selections[i]+" and correct index>"+correctIndex);
      if (selections[i] === correctIndex) {
        numCorrect++;
      }
    }
    
    score.append('<pre>'+'You got ' + numCorrect + ' questions out of ' +
    		obtainedQuestion.length + ' right!!!'+'</pre>');
    return score;
  }
  
//Click handler for the 'Submit Score' button
  $('#score').on('click', function (e) {
    e.preventDefault();
    
    var socreBoard = {
    		"score":numCorrect,
    		"user":user,
    		"tags":tag
    };
    if(quiz.is(':animated')) {
      return false;     
    } 
  //ajax call
    $.ajax({
    	  type: "POST",
    	  url: '/submit/score',
    	  contentType : "application/json",
    	  data : JSON.stringify(socreBoard),
    	  dataType : 'json',
    	  cache: false,    	    	 
    	  processData: false,
    	  success: function(result) {

    	    console.log("after score submit",result);
    		  if(result.status){
    			  alert('Congratulations!!!!\nYour Score has been submitted successfully.');
                  window.location = "/home/";
					$("#postResultDiv").html("<p style='background-color:#7FA7B0; color:white; padding:20px 20px 20px 20px'>" + 
												"Post Successfully! <br>" +
												"---> Customer's Info: FirstName = " + 
												result.data.user + " ,LastName = " + result.data.score + "</p>");
				}else{
					$("#postResultDiv").html("<strong>Error</strong>");
				}
              console.log('SUCCESS: ', result);
          },
          error: function(result) {
              console.log('ERROR: ', result);
          },
    	});
    
    
    $('#submit').hide();
  });
}