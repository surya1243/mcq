package com.it.mcq.message;

public class ResponseMessage {
	private String message;
	private Object data;
	private boolean status;
	
	public ResponseMessage(){
		
	}
	
	public ResponseMessage(String message, Object data, boolean status){
		this.status = status;
		this.data = data;
		this.message=message;
	}
 
	public boolean getStatus() {
		return status;
	}
 
	public void setStatus(boolean status) {
		this.status = status;
	}
 
	public Object getData() {
		return data;
	}
 
	public void setData(Object data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
