package com.it.mcq.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.it.mcq.message.ResponseMessage;
import com.it.mcq.model.UserScore;
import com.it.mcq.repository.UserScoreRepository;
import com.it.mcq.service.ScoreDetailServiceImpl;

@RestController
public class ScoreBoardController {
	public static final int ID_LENGTH = 5;
	
	@Autowired
	private UserScoreRepository userScoreRepository;
	
	@Autowired
	private ScoreDetailServiceImpl scoreService;
		
	@RequestMapping(value="/submit/score", method=RequestMethod.POST)
	public ResponseMessage saveUserScore(@RequestBody UserScore userScore, BindingResult bindingResult) {
		
		//set new id for user score\
		userScore.setId(generateUniqueId());
		System.err.println("User Score"+userScore);
		UserScore savedUserScore=scoreService.saveScore(userScore);
		System.err.println("New saved score"+savedUserScore.getScore());
		System.err.println("New saved score"+savedUserScore.getUser());
		ResponseMessage response = new ResponseMessage("Done", savedUserScore,true);
		return response;
		}
	
	public String generateUniqueId() {
        return RandomStringUtils.randomAlphanumeric(ID_LENGTH);
    }
	
	@SuppressWarnings("unchecked")
	@GetMapping(value = "/count")
	public Map<String, Integer> getUniqueGenericClassesWithCount() {
		Map map = new HashMap<String, Integer>();
		
		userScoreRepository.findAll().forEach(userScore->
		{
			if(map.containsKey(userScore.getUser()))
					{
						int max= getMax(userScore.getScore(),(Integer) map.get(userScore.getUser()));
						map.remove(userScore.getUser());
						map.put(userScore.getUser(),max);
					}
			else
				map.put(userScore.getUser(), userScore.getScore());
		});
		return map;
	}
	
	private int getMax(int num1,int num2)
	{
		return num1>=num2?num1:num2;
	}
	
}
