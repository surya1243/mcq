package com.it.mcq.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.it.mcq.model.Question;
import com.it.mcq.service.TutorialPointTagsFetchServiceImpl;
import com.it.mcq.service.TutorialsPointQuestionFetchServiceImpl;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.it.mcq.model.AppUser;
import com.it.mcq.repository.UserRepository;
import com.it.mcq.service.UserDetailServiceImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Controller
public class HomeController {
	
	public static final int ID_LENGTH = 5;
	
	@Autowired
	private UserRepository userRepository;
	 @Autowired
	    TutorialPointTagsFetchServiceImpl tagsFetchService;
	
	@Autowired
	private UserDetailServiceImpl userService;

	@Autowired
	TutorialsPointQuestionFetchServiceImpl questionFetchService;
	
		
	@RequestMapping(value = { "/", "/login" }, method = RequestMethod.GET)
	public String getIndex(HttpServletRequest request, Model model) {
		if (request.getRemoteUser() != null) {
			return "redirect:/home";
		}
		return "webindex";
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String getRegister() {
		return "register";
	}
	
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView getDashboard() throws IOException {
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.addObject("tags", tagsFetchService.getAvailableTags());
		modelAndView.addObject("message", tagsFetchService.getAvailableTags());
		modelAndView.setViewName("home");
		return modelAndView;
	}

	
	@RequestMapping(value = "/question", method = RequestMethod.GET)
	public ModelAndView getQuestion(HttpSession session) {
		//TODO remove and assign tags from request
		List<String> tagList = (List<String>)session.getAttribute("tags");

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("question");
		List<Question> questionList=getQuestions(tagList);
		modelAndView.addObject("questions",questionList);
	

		return modelAndView;
	}
	@RequestMapping(value = "/question", method = RequestMethod.POST)
	public String showQuestion(@RequestParam("languages") List<String> selectedLanguages, HttpSession session) {
		session.setAttribute("tags",selectedLanguages);

		//set expiry for session
		session.setMaxInactiveInterval(60*60);
		return "redirect:/question";
	}
	
	
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public ModelAndView saveUserDetail(AppUser appUser, BindingResult bindingResult) {
		
		System.err.println("Registering new user");
		ModelAndView modelView = new ModelAndView();
		System.out.println("hello");
		AppUser userExists = userService.findUserByEmail(appUser.getEmail());
        if (userExists != null) {
        	System.err.println("Registered user"+userExists);
            bindingResult
                    .rejectValue("email", "error.user",
                            "There is already a user registered with the username provided");
        }
        else {

		AppUser savedUser=userService.saveUser(appUser);
		System.err.println("New saved usrr"+savedUser);
		modelView.setViewName("webindex");
		}
		
		if(bindingResult.hasErrors()) {
			modelView.addObject("message", "Could not Register the new User");
			modelView.setViewName("webindex");
		}
		
		return modelView;
		}

	
private List<Question> getQuestions(List<String> tags)
{
	List<Question> questionList = new LinkedList<>();
	tags.forEach(tag->{
		//pre process tags
		String processedTag=tag.replaceAll(" ", "").toLowerCase();
		questionList.addAll(questionFetchService.fetchQuestion(processedTag));
	});
	return questionList;
}


}

