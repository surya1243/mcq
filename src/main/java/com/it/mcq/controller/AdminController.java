package com.it.mcq.controller;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.it.mcq.service.UserDetailService;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import com.it.mcq.model.AppUser;
import com.it.mcq.model.UserScore;
import com.it.mcq.model.UserScoreBinder;
import com.it.mcq.repository.UserScoreRepository;


@Controller
@RequestMapping("/admin")
public class AdminController {
	
	public static final int ID_LENGTH = 5;

	
	@Autowired
	private UserScoreRepository scoreRepository;


	@Autowired
	UserDetailService userService;
	
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public String getDashboard(HttpServletRequest request, Model model) {

		return "admin/dashboard";
	}
	
	@RequestMapping(value = "/score", method = RequestMethod.GET)
	public String getScoreBoard(HttpServletRequest request, Model model, UserScore score) {
		Iterable<UserScore> scoreList = scoreRepository.findAll();
		
		
		List<UserScoreBinder> displayList = new LinkedList<>();
		scoreList.forEach(userScore->
		{UserScoreBinder binder = new UserScoreBinder();
			binder.setScore(userScore);
			binder.setAppUser(userService.findUserByEmail(userScore.getUser()));
			displayList.add(binder);
		});
		System.out.println(displayList);
		model.addAttribute("scoreList", displayList);		
		return "admin/scoreboard";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getAdminLogin(HttpServletRequest request, Model model) {

		return "admin/login";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String getAdminRegisterPage(HttpServletRequest request, Model model) {

		return "admin/registeradmin";
	}
	
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public ModelAndView saveAdminDetail(AppUser admin, BindingResult bindingResult) {
		ModelAndView modelView = new ModelAndView();
		AppUser userExists = userService.getUserByEmailAndRole(admin.getEmail(),"ADMIN");
        if (userExists != null) {
            bindingResult
                    .rejectValue("email", "error.user",
                            "There is already a user registered with the username provided");
        }
		if(bindingResult.hasErrors()) {
			modelView.addObject("message", "Could not Register the new User");
			modelView.setViewName("admin/registeradmin");
		}
		else {
		userService.saveAdmin(admin);
		modelView.setViewName("admin/registeradmin");
		}
		
		return modelView;
		}
	

	@RequestMapping(value="/adminlist", method=RequestMethod.GET)
	public String getAdminList(Model model) {
		List<AppUser> adminList = userService.findAllUserByRoleAdmin();
		System.out.println(adminList);
		model.addAttribute("adminList", adminList);
		return "admin/adminlist";
	}
	public String generateUniqueId() {
        return RandomStringUtils.randomAlphanumeric(ID_LENGTH);
    }

}
