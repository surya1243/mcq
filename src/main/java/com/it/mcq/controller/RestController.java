package com.it.mcq.controller;


import com.it.mcq.message.ResponseMessage;
import com.it.mcq.model.*;
import com.it.mcq.service.ScoreDetailService;
import com.it.mcq.service.TutorialPointTagsFetchServiceImpl;
import com.it.mcq.service.TutorialsPointQuestionFetchServiceImpl;
import com.it.mcq.service.UserDetailServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
@ComponentScan("com.it.mcq.core.scrapper")
public class RestController {
	
	@Autowired
	private UserDetailServiceImpl userService;

    @Autowired
    TutorialsPointQuestionFetchServiceImpl questionFetchService;

    @Autowired
    TutorialPointTagsFetchServiceImpl tagsFetchService;
    @Autowired
    ScoreDetailService scoreService;

    @GetMapping
    @RequestMapping(value = "/api/question/{tag}")
    public List<Question> getQuestion(@PathVariable(value="tag") String tag)
    {
        tag.replaceAll(" ", "");
        return questionFetchService.fetchQuestion(tag.toLowerCase());
    }
    @PostMapping
    @RequestMapping(value = "/api/question")
    public List<Question> getQuestionsFromTags(@RequestParam("tags") List<String> tags)
    {
        System.out.println(tags);
        List<Question> questionList = new LinkedList<>();
        tags.forEach(tag->{
            //pre process tags
            String processedTag=tag.replaceAll(" ", "").toLowerCase();
            questionList.addAll(questionFetchService.fetchQuestion(processedTag));
        });
        return questionList;
    }

    @GetMapping
    @RequestMapping(value = "/api/tags")
    public List<String> getPossibleTags() throws IOException
    {
        return tagsFetchService.getAvailableTags();
    }
    
    @PostMapping
    @RequestMapping(value = "/api/register", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseMessage registerUser(@RequestParam("firstName") String firstName , @RequestParam("lastName") String lastName,
                                        @RequestParam("email") String email, @RequestParam("password") String password, @RequestParam("address") String address)
    {	
    	AppUser appUser= new AppUser();
    	appUser.setAddress(address);
    	appUser.setEmail(email);
    	appUser.setFirstName(firstName);
    	appUser.setLastName(lastName);
    	appUser.setPassword(password);

		AppUser userExists = userService.findUserByEmail(appUser.getEmail());

        if (userExists != null) {

            return new ResponseMessage(
                    "There is already a user registered with the username provided",
                    null,
                    false
            );
        }
        else {
		AppUser savedUser=userService.saveUser(appUser);
            return new ResponseMessage("Success", savedUser, true);
		}		

    }
    
    @PostMapping
    @RequestMapping(value = "/api/login")
    public ResponseMessage makeLogin(@RequestParam("email") String email , @RequestParam("password") String password)
    {
		AppUser appUser =userService.findUserByEmail(email);
		boolean exists = userService.doesUserExists(email,password);
        if (exists) {

            return new ResponseMessage("Success",appUser,true);
        }
        else {
            return new ResponseMessage("Couldn't Login",null,false);
		}

    }
    
    @PostMapping
    @RequestMapping(value = "/api/getquestion")
    public List<Question> showQuestion(@RequestParam("tag") List<String> tags)
    {
    	 List<Question> questionList = new LinkedList<>();
         tags.forEach(tag->{
             String processedTag=tag.replaceAll(" ", "").toLowerCase();
             questionList.addAll(questionFetchService.fetchQuestion(processedTag));
         });
         return questionList;

    }
    @PostMapping
    @RequestMapping(value = "/api/submitscore")
    public ResponseMessage makeLogin(@RequestParam("email") String email ,@RequestParam("score") int score,@RequestParam("tags") List<String> tags)
    {
        UserScore userScore = new UserScore();
        userScore.setScore(score);
        userScore.setTags(tags);
        userScore.setUser(email);

        //save this score
        UserScore savedScore=scoreService.saveScore(userScore);
        if(savedScore!=null)
        {
            return new ResponseMessage("Success",savedScore,true);
        }
        else{
            return new ResponseMessage("error while saving score",null,false);
        }

    }

  
}
