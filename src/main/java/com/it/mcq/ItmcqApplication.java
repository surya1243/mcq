package com.it.mcq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.elasticsearch.client.Client;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import java.net.UnknownHostException;
import java.net.InetAddress;
@SpringBootApplication
@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class})
public class ItmcqApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItmcqApplication.class, args);
	}
	
	 @Bean
	    public Client elasticsearchClient() throws UnknownHostException {
	        TransportClient client =
	                new PreBuiltTransportClient(Settings.EMPTY)
	                        .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));

	        return client;
	    }

	    @Bean
	    public ElasticsearchTemplate elasticsearchTemplate() throws Exception {
	        return new ElasticsearchTemplate(elasticsearchClient());
	    }
}
