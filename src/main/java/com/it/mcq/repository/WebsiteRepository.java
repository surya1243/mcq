package com.it.mcq.repository;

import com.it.mcq.model.WebSite;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;


public interface WebsiteRepository extends ElasticsearchRepository<WebSite, String> {
}
