package com.it.mcq.repository;


import com.it.mcq.model.Question;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface QuestionRepository extends ElasticsearchRepository<Question, String> {
        List<Question> findQuestionByTagIsLike(String tag);
        List<Question> findQuestionsByTag(String tag);
        List<Question> findAllByTag(String tag);
}
