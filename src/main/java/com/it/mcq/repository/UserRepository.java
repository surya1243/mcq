package com.it.mcq.repository;

import com.it.mcq.model.AppUser;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


import java.util.List;

public interface UserRepository extends ElasticsearchRepository<AppUser, String> {
	AppUser findByEmail(String email);
	List<AppUser> findAll();
	AppUser findByEmailAndRole(String email,String role);
	List<AppUser> getAppUsersByRole(String role);
}
