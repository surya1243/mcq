package com.it.mcq.repository;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.it.mcq.model.UserScore;



public interface UserScoreRepository extends ElasticsearchRepository<UserScore, String> {
	
	UserScore findByUser(String user); 
	String findTopByScoreAndUserContaining(String user);

}
