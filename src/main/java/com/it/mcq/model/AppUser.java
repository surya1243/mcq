package com.it.mcq.model;

import java.util.Set;


import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Document(indexName = "user", type = "user", shards = 1)
public class AppUser{
	
	//@Id ObjectId databaseId;	
	@Id
    private String id;
    private String firstName;
    private String lastName;
    private boolean enabled;
    private String email;
    private String password;
    private String address;
    private String role;
    
    public AppUser() {
 
    }
  
    public String getId() {
        return id;
    }
 
    public void setId(String userId) {
        this.id = userId;
    }
 
    public String getFirstName() {
        return firstName;
    }
 
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
 
    public String getLastName() {
        return lastName;
    }
 
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
 
    public boolean isEnabled() {
        return enabled;
    }
 
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
 
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }
 
    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getRole() {
        return role;
    }

    public void setRole(String roles) {
        this.role = roles;
    }

	@Override
	public String toString() {
		return "AppUser [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", enabled=" + enabled
				+ ", email=" + email + ", password=" + password + ", address=" + address + ", role=" + role + "]";
	}

}