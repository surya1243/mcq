package com.it.mcq.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.regex.Pattern;

@Document(indexName = "website", type = "website", shards = 1)
public class WebSite {
    @Id
    private String id;
    private String url;


    public WebSite(){}
    public WebSite(String url,String id) {
        this.url = url;
        this.id=id;
    }
    public WebSite(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean contentFrom(String siteName)
    {
        return Pattern.compile(Pattern.quote(siteName), Pattern.CASE_INSENSITIVE).matcher(url).find();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "WebSite{" +
                "url='" + url + '\'' +
                ", id=" + id +
                '}';
    }
    @Override
    public boolean equals(Object object) {
        WebSite toCompare;
        if(object instanceof WebSite) toCompare=(WebSite) object;
        else return false;

       if(url.equalsIgnoreCase(toCompare.getUrl())) return true;
       return false;

    }
}
