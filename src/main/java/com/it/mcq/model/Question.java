package com.it.mcq.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import java.util.List;


@Document(indexName = "questions", type = "questions", shards = 1)

public class Question {
    @Id
    private String id;
    private String questionString;
    private String tag;
    private String correct_answer;
    private List<String> options;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestionString() {
        return questionString;
    }

    public void setQuestionString(String question) {
        this.questionString = question;
    }

    public String getCorrect_answer() {
        return correct_answer;
    }

    public void setCorrect_answer(String correct_answer) {
        this.correct_answer = correct_answer;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Question(String id, String question, String tag, String correct_answer, List<String> options) {
        this.id = id;
        this.questionString = question;
        this.tag = tag;
        this.correct_answer = correct_answer;
        this.options=options ;
    }
    public Question(){}

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", questionString='" + questionString + '\'' +
                ", tag='" + tag + '\'' +
                ", correct_answer='" + correct_answer + '\'' +
                ", options=" + options +
                '}';
    }
}
