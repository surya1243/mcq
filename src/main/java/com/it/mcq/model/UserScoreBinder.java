package com.it.mcq.model;

public class UserScoreBinder {
	private AppUser appUser;
	private UserScore score;
	
	
	public Object getAppUser() {
		return appUser;
	}


	public void setAppUser(AppUser appUser) {
		this.appUser = appUser;
	}


	public UserScore getScore() {
		return score;
	}


	public void setScore(UserScore score) {
		this.score = score;
	}


	@Override
	public String toString() {
		return "UserScoreBinder [appUser=" + appUser + ", score=" + score + "]";
	}
}
