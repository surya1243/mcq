package com.it.mcq.config;



import com.it.mcq.repository.QuestionRepository;
import com.it.mcq.repository.UserRepository;
import com.it.mcq.repository.WebsiteRepository;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.net.InetAddress;


@Configuration
@EnableElasticsearchRepositories(basePackages = "com.it.mcq.repository")

public class ElasticConfiguration {

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    WebsiteRepository websiteRepository;

    @Autowired
    UserRepository userRepository;

    @Value("${elasticsearch.host}")
    private String EsHost;

    @Value("${elasticsearch.port}")
    private int  EsPort;

    @Value("${elasticsearch.clustername}")
    private String EsClusterName;


    @Bean
    public Client client() throws Exception {
        Settings elasticsearchSettings = Settings.builder()
                .put("client.transport.sniff", true)
                .put("cluster.name", EsClusterName).build();
        TransportClient client = new PreBuiltTransportClient(elasticsearchSettings);
        if(EsPort==0) EsPort=9300;
        client.addTransportAddress(new TransportAddress(InetAddress.getByName(EsHost), EsPort));
        return client;
    }
}