package com.it.mcq.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.it.mcq.model.UserScore;
import com.it.mcq.repository.UserScoreRepository;
@Service
public class ScoreDetailServiceImpl implements ScoreDetailService {
	
	private UserScoreRepository scoreRepository;
	
	@Autowired
    public ScoreDetailServiceImpl(UserScoreRepository scoreRepository) {
        this.scoreRepository = scoreRepository;
    }


	@Override
	public UserScore getScoreByUser(String user) {
		return scoreRepository.findByUser(user);
	}

	@Override
	public List<UserScore> getAllScore() {
		List<UserScore> scoreList = new ArrayList<>();
		scoreRepository.findAll().forEach(scoreList::add);
		return scoreList;
	}

	@Override
	public UserScore saveScore(UserScore score) {
		scoreRepository.save(score);
	    return scoreRepository.save(score);
	}

}
