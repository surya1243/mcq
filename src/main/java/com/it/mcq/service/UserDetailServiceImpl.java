package com.it.mcq.service;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import com.it.mcq.util.RandomKeyGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.it.mcq.model.AppUser;
import com.it.mcq.repository.UserRepository;



@Service
public class UserDetailServiceImpl implements UserDetailService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	PasswordEncoder encoder;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		final AppUser appUserFromDb=userRepository.findByEmail(email);

		if (appUserFromDb == null)
			throw new UsernameNotFoundException(email);

		UserDetails userDetail = new UserDetails() {
			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {

				Collection<GrantedAuthority> authority = new LinkedList<>();
				authority.add(new GrantedAuthority() {
					@Override
					public String getAuthority() {
						return appUserFromDb.getRole();
					}
				});
				return authority;
			}

			@Override
			public String getPassword() {
				return appUserFromDb.getPassword();
			}

			@Override
			public String getUsername() {
				return appUserFromDb.getEmail();
			}

			@Override
			public boolean isAccountNonExpired() {
				return true;
			}

			@Override
			public boolean isAccountNonLocked() {
				return true;
			}

			@Override
			public boolean isCredentialsNonExpired() {
				return true;
			}

			@Override
			public boolean isEnabled() {
				return appUserFromDb.isEnabled();
			}
		};
		return userDetail;
	}



	@Override
	public AppUser findUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public Optional<AppUser> getUserById(String id) {

		return userRepository.findById(id);
	}

	@Override
	public AppUser getUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public List<AppUser> getAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public AppUser saveUser(AppUser user) {
		user.setRole("USER");
		return save(user);
	}

	@Override
	public AppUser saveAdmin(AppUser user) {
		user.setRole("ADMIN");
		return save(user);
	}


	@Override
	public AppUser getUserByEmailAndRole(String email, String role) {
		return userRepository.findByEmailAndRole(email,role);
	}

	private AppUser save(AppUser user)
	{
		user.setEnabled(true);
		user.setId(RandomKeyGenerator.generateUniqueId());
		user.setPassword( encoder.encode(user.getPassword()));
		return userRepository.save(user);
	}



	@Override
	public List<AppUser> findAllUserByRoleAdmin() {
		return userRepository.getAppUsersByRole("ADMIN");
	}



	@Override
	public List<AppUser> findAllUserByRoleUser() {
		return userRepository.getAppUsersByRole("USER");
	}



	@Override
	public boolean doesUserExists(String email,String unhashedPassword) {
		AppUser userFromDB =userRepository.findByEmailAndRole(email, "USER");
		if(userFromDB==null) return false;
		return encoder.matches(unhashedPassword, userFromDB.getPassword());
		
	}
}