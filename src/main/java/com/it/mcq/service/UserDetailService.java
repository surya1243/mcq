package com.it.mcq.service;
import java.util.List;
import java.util.Optional;

import com.it.mcq.model.AppUser;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserDetailService  extends UserDetailsService {


    Optional<AppUser> getUserById(String id);
    AppUser getUserByEmail(String email);
    AppUser getUserByEmailAndRole(String email,String role);
    List<AppUser> getAllUsers();
    AppUser saveUser(AppUser user);
    AppUser saveAdmin(AppUser user);
    AppUser findUserByEmail(String email);
    
    List<AppUser> findAllUserByRoleAdmin();
    List<AppUser> findAllUserByRoleUser();
    
    boolean doesUserExists(String email,String unhashedPassword);

}
