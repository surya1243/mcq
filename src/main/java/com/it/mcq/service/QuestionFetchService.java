package com.it.mcq.service;

import com.it.mcq.model.Question;

import java.util.List;

public interface QuestionFetchService {
    List<Question> fetchQuestion(String tag);
}
