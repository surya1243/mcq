package com.it.mcq.service;

import com.it.mcq.core.scrapper.TutorialPointCategories;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class TutorialPointTagsFetchServiceImpl implements TagsFetchService{
    @Override
    public List<String> getAvailableTags() throws IOException {
        TutorialPointCategories tutorialPointCategories = new TutorialPointCategories();
        return tutorialPointCategories.fetchSupportedTags();
    }
}
