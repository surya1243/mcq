package com.it.mcq.service;

import com.it.mcq.model.Question;
import com.it.mcq.model.WebSite;
import com.it.mcq.repository.QuestionRepository;
import com.it.mcq.repository.WebsiteRepository;
import com.it.mcq.core.scrapper.TutorialPointCategories;
import com.it.mcq.core.scrapper.TutorialsPointContentFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;



@Component
public class TutorialsPointQuestionFetchServiceImpl implements QuestionFetchService {

    @Autowired
    WebsiteRepository websiteRepository;

    @Autowired
    QuestionRepository questionRepository;

    @Override
    public List<Question> fetchQuestion(String tag) {
        try{
        	
        	System.out.println("in Question fEcth Servece");
            //create new Link Lit to store list of websites to visit
            List<WebSite> visitedWebsiteList=new LinkedList<>();

            //assign value to linklist
            websiteRepository.findAll().forEach(visitedWebsiteList::add);

            //create core objects
            TutorialsPointContentFetcher contentFetcher = new TutorialsPointContentFetcher();
            TutorialPointCategories categoriesLinkGenerator =new TutorialPointCategories();

            //get links to website that contains questions to required tag
            List<WebSite> questionsLinks=categoriesLinkGenerator.fetchLink(tag);

            for (WebSite link:questionsLinks)
            {
                if(visitedWebsiteList.contains(link))
                continue;

                List<Question> fetchedQuestionList=contentFetcher.fetchQuestion(link.getUrl());
                fetchedQuestionList.forEach(question ->question.setTag(tag));
                Iterable<Question> saved =questionRepository.saveAll(fetchedQuestionList);
                System.err.println("Saved:\n"+saved);
                websiteRepository.save(link);
                }

            return questionRepository.findAllByTag(tag);
        }
        catch(IOException e)
        {
            System.err.println(e.getMessage());
        }

        return null;
    }


}
