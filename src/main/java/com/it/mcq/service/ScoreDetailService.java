package com.it.mcq.service;

import java.util.List;
import java.util.Optional;

import com.it.mcq.model.AppUser;
import com.it.mcq.model.UserScore;

public interface ScoreDetailService {
	
;

	UserScore getScoreByUser(String user);

    List<UserScore> getAllScore();

    UserScore saveScore(UserScore user);

}
