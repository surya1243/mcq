package com.it.mcq.service;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;


public interface TagsFetchService {
    List<String> getAvailableTags() throws IOException;
}
