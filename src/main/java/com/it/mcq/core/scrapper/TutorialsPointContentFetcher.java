package com.it.mcq.core.scrapper;


import com.it.mcq.model.Question;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class TutorialsPointContentFetcher implements ContentFetcher {

    private static String TAG_P="p";
    private static String TAG_PRE="pre";
    private  static String CSS_CLASS_TRUE="true";
    private static String CSS_CLASS_FALSE="false";
    @Override
    public List<Question> fetchQuestion(String url) throws IOException {
        Document doc = Jsoup.connect(url).get();
        Elements elements =doc.getElementsByClass("Q");
        return parseQuestions(elements);
    }

    private List<Question> parseQuestions(Elements elements)
    {
        List<Question> questionsList = new LinkedList<>();
        elements.forEach(element ->questionsList.add(parseQuestion(element)));
        return questionsList;
    }
    private Question parseQuestion(Element element) throws ArrayIndexOutOfBoundsException
    {
        Question question = new Question();

        Elements values=element.getElementsByTag(TAG_P);
        //Elements falseAnswer=element.getElementsByClass(CSS_CLASS_FALSE);
        Elements trueAnswer =element.getElementsByClass(CSS_CLASS_TRUE);
        Elements preTagsElement=element.getElementsByTag(TAG_PRE);

        Element questions =values.first();

        if((preTagsElement!=null)&&(preTagsElement.size()>0))
        {
            String questionTxt=textFormatter(questions.text());
            String codeTxt=preTagsElement.get(0).text();
            question.setQuestionString(questionTxt+"\n"+codeTxt);

        }
        else
            question.setQuestionString(textFormatter(questions.text()));
        List<String> options= new LinkedList<>();
        for(int index =1;index<values.size();index++)
        {
            Element tempElement=values.get(index);

            options.add(textFormatter(tempElement.text()));
        }


        question.setCorrect_answer(textFormatter(trueAnswer.text()));
        question.setOptions(options);
        question.setCorrect_answer(textFormatter(trueAnswer.text()));
        System.out.println(question);
        return question;
    }
    private String textFormatter(String unformattedString ) throws ArrayIndexOutOfBoundsException
    {
        return unformattedString.split("-")[1].replace(".", "");
    }

}
