package com.it.mcq.core.scrapper;



import com.it.mcq.model.Question;

import java.io.IOException;
import java.util.List;

public interface ContentFetcher {
    public List<Question> fetchQuestion(String url) throws IOException;

}
