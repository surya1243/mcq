package com.it.mcq.core.scrapper;


import com.it.mcq.model.WebSite;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

import java.util.LinkedList;
import java.util.List;


public class TutorialPointCategories {
    private String url="https://www.tutorialspoint.com/questions_and_answers.htm";

    public List<WebSite> fetchLink(String tag) throws IOException {
        Document doc = Jsoup.connect(url).get();
        Elements elements =doc.getElementsByClass("col-md-3").get(1).getElementsByClass("menu");
        return parseLinks(elements.first(),tag);
    }

    private List<WebSite> parseLinks(Element element,String tag)
    {
        List<WebSite>  linksList= new LinkedList<>();
        Elements listTags=element.getElementsByTag("li");

        listTags.forEach(element1 -> {
            Element aTag=element1.getElementsByTag("a").get(0);
            String url=aTag.attr("href");
            if(url.contains(tag))
                linksList.add(new WebSite(makeQualifiedUrl(url)));
        });

        return linksList;
    }
    public List<String> fetchSupportedTags() throws IOException
    {
        Document doc = Jsoup.connect(url).get();
        Elements elements =doc.getElementsByClass("col-md-3").get(1).getElementsByClass("menu");
        return parseTags(elements.first());
    }
    private List<String> parseTags(Element element)
    {
        List<String>  tagList= new LinkedList<>();
        Elements listTags=element.getElementsByTag("li");

        listTags.forEach(element1 -> {
            Element aTag=element1.getElementsByTag("a").get(0);
            tagList.add(aTag.text());
        });

        return tagList;
    }

private static String makeQualifiedUrl(String url)
{
    return "https://www.tutorialspoint.com"+url;
}

}
