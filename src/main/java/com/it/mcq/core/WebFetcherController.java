package com.it.mcq.core;


import com.it.mcq.model.Question;
import com.it.mcq.repository.WebsiteRepository;
import com.it.mcq.core.scrapper.ContentFetcher;
import com.it.mcq.core.scrapper.TutorialsPointContentFetcher;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import com.it.mcq.core.crawler.WebCrawler;
import com.it.mcq.repository.QuestionRepository;
import com.it.mcq.model.WebSite;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;


@Service
public class WebFetcherController {
    private WebCrawler webCrawler;
    private ContentFetcher contentFetcher;

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    WebsiteRepository websiteRepository;



    public WebFetcherController()
    {

    }
/*
*
* this function return boolean
* if TRUE then content from website were fetched and added to repo
* id FALSE then content from web were not found */
public boolean searchQuestion(String tag) throws IOException
{
    webCrawler =new WebCrawler(tag);
    String url=webCrawler.getRootUrl();
    WebSite site= new WebSite(url);

    contentFetcher =this.factoryContentFetcher(site);
    if(contentFetcher==null) throw new IOException("pattern for"+url+" not constructed");

    if(!isAlreadyVisitedWebSite(site))
    {
        //save visited website
        saveWebsite(site);

        //fetch questions from site
        List<Question> questionList =contentFetcher.fetchQuestion(site.getUrl());

        //assign tag to each question
        questionList.forEach(question-> question.setTag(tag));

        //save question list
        saveQuestionsToRepository(questionList);
        return true;
    }
    return false;

}
    /*
    * This method is factory pattern for ContentFetcher*/
    private ContentFetcher factoryContentFetcher(WebSite website)
    {
        ContentFetcher contentFetcher;

        if(website.contentFrom("tutorialspoint"))
            contentFetcher = new TutorialsPointContentFetcher();
        else
            contentFetcher = null;
        return contentFetcher;
    }

    private void saveQuestionsToRepository(List<Question> questionList)
    {
        questionRepository.saveAll(questionList);
    }

    private void saveWebsite(WebSite site){
         websiteRepository.save(site);
    }

    private boolean isAlreadyVisitedWebSite(WebSite site)
    {
        if(websiteRepository==null) {System.out.println("websiteRepo is null"); return false;}
        List<WebSite> visitedSites=iterableToList(websiteRepository.findAll());

        for (WebSite visitedSite:visitedSites) {
            if(visitedSite.equals(site)) return true;
        }
       return false;
    }

    List<WebSite> iterableToList(Iterable<WebSite> objectIterable)
    {
        List returnList = new LinkedList();
        objectIterable.forEach(returnList::add);
        return returnList;
    }

}
