package com.it.mcq;

import com.it.mcq.model.AppUser;
import com.it.mcq.repository.QuestionRepository;
import com.it.mcq.repository.UserRepository;
import com.it.mcq.core.WebFetcherController;
import com.it.mcq.core.crawler.TutorialsPointWebCrawler;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ItmcqApplicationTests {
	@Autowired
	QuestionRepository questionRepository;
	@Autowired
	WebFetcherController webFetcherController;
	@Autowired
	UserRepository userRepository;

	@Test
	public void contextLoads() {
		AppUser user = new AppUser();
		user.setEmail("abhay@gmail.com");
		user.setFirstName("Abhay");
		user.setLastName("Tiwari");
		user.setPassword("11111");
		user.setId("1234");
		AppUser savedUser =userRepository.save(user);
		assertEquals(user,savedUser);
	}

	@Test
	public void scrapperWorksProperly() throws IOException
	{

		webFetcherController.searchQuestion("java");
		System.out.println(questionRepository.findAll());
	}
	@Test
	public void crawlerWorksProperly()
	{

	}
	public void tutorialsPointWebCrawlerWorksProperly() throws Exception
	{
        String crawlStorageFolder = "/data/crawl/root";
        int numberOfCrawlers = 1;

        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder(crawlStorageFolder);

        /*
         * Instantiate the controller for this crawl.
         */
        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);

        /*
         * For each crawl, you need to add some seed urls. These are the first
         * URLs that are fetched and then the crawler starts following links
         * which are found in these pages
         */
        controller.addSeed("https://www.tutorialspoint.com/java");


        /*
         * Start the crawl. This is a blocking operation, meaning that your code
         * will reach the line after this only when crawling is finished.
         */

        controller.start(TutorialsPointWebCrawler.class, numberOfCrawlers);
	}

}
