package com.it.mcq;


import com.it.mcq.core.scrapper.TutorialPointCategories;
import com.it.mcq.core.scrapper.TutorialsPointContentFetcher;
import com.it.mcq.model.WebSite;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WebCrawlerTests {
    @Autowired
    TutorialsPointContentFetcher contentFetcher;
    @Test
    public void webScrapperWorksForCategories() throws IOException{
        String tag="java";
        TutorialPointCategories categoriesLinkGenerator =new TutorialPointCategories();
        List<WebSite> questionsLinks=categoriesLinkGenerator.fetchLink(tag);
        System.out.println(questionsLinks);

    }
    @Test
    public void webScrapperWorksForTutorialPoint() throws IOException{
        String url="https://www.tutorialspoint.com/java8/java8_online_quiz.htm";
        System.err.println(contentFetcher.fetchQuestion(url));
    }
}
